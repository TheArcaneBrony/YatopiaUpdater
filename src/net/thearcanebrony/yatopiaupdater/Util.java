package net.thearcanebrony.yatopiaupdater;

import net.thearcanebrony.yatopiaupdater.ostype.OS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Util {
    public static void StartProcess(String command){
        ArrayList<String> exec = new ArrayList<>(Arrays.asList(OS.getOS().ShellCmd));
        Console.logDebug(String.format("Detected OS: %s (%s)!", OS.getOS().Name, OS.getOS()));
        Console.logDebug(String.format("Using \"%s\" as shell!", String.join(" ", exec)));
        exec.add(command);
        final ProcessBuilder p = new ProcessBuilder(exec);
        p.inheritIO();
        try {
            Console.WriteLine(String.format("Launching command: %s", command));
            p.start().waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
}
