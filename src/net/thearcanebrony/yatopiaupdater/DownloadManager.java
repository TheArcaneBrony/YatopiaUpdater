package net.thearcanebrony.yatopiaupdater;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.Arrays;

public class DownloadManager {
    private static DecimalFormat df = new DecimalFormat("#.##");
    public static String GetUrlResponse(String _url) {
        Console.logDebug(String.format("Sending HTTP request to %s!", _url));
        try {
            URL url = new URL(_url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuffer response = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            Console.LogIO(response.toString());
            return response.toString();
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Console.WriteLine("Something went wrong!\n" + sw);
            return "Something went wrong!";
        }
    }
    private static long totalBytes = 0;
    public static long DownloadFileNew(String _url, String outfile) {
//        long totalBytes = 0;
        long updateTresh = 1;
        long filesize = getFileSize(_url);
        try {
            totalBytes = 0;
            BufferedInputStream in = new BufferedInputStream(new URL(_url).openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(outfile);
            byte dataBuffer[] = new byte[1024];
            int bytesRead;
            new Thread(){
                @Override
                public void run() {
                    String[] Spinner = "|/-\\".split("");
                    String e = "              ";
                    int cooldown = 50;
                    double ups = 1000d/cooldown;
                    double[] _dls = new double[(int) Math.ceil(ups)];
                    int i = 0;
                    int SP = 0;
                    long lastUpdate = 0;
                    while(totalBytes < filesize){
                        String sp = Spinner[SP++%Spinner.length];
                        double pct = totalBytes/(double)filesize*100;
                        _dls[(int) (i++%ups)] = totalBytes - lastUpdate;
                        double dls = Arrays.stream(_dls).sum();
                        if(Config.LogIO){
                            Console.Write(String.format("%s Read %s/%s bytes (%.2f%%) at %s bytes/s%s\r", sp, totalBytes, filesize, pct, dls, e));
                        }
                        else {
                            Console.Write(String.format("%s Read %.2f/%.2f MB (%.2f%%) at %.2f MB/s%s\r", sp, totalBytes/1024d/1024d, filesize/1024d/1024d, pct, dls/1024d/1024d, e));
                        }
                        lastUpdate = totalBytes;
                        try {
                            Thread.sleep(cooldown);
                        } catch (InterruptedException ie) {
                            Thread.currentThread().interrupt();
                        }
                    }
                }
            }.start();
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
                totalBytes += bytesRead;
            }

            Console.WriteLine(String.format("Download complete! Downloaded %s into %s!",Config.LogIO?(totalBytes + " bytes"):(String.format("%.2f", filesize/1024d/1024d) + " MB"),outfile));
            return totalBytes;
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Console.WriteLine("Something went wrong!\n" + sw);
            return totalBytes;
        }
    }
    private static long getFileSize(String _url) {
        URLConnection conn = null;
        try {
            URL url = new URL(_url);
            conn = url.openConnection();
            if(conn instanceof HttpURLConnection) {
                ((HttpURLConnection)conn).setRequestMethod("HEAD");
            }
            conn.getInputStream();
            return conn.getContentLengthLong();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if(conn instanceof HttpURLConnection) {
                ((HttpURLConnection)conn).disconnect();
            }
        }
    }
}
