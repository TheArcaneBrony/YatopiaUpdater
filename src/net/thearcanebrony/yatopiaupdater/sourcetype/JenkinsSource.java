package net.thearcanebrony.yatopiaupdater.sourcetype;

import net.thearcanebrony.yatopiaupdater.Config;
import net.thearcanebrony.yatopiaupdater.Console;
import net.thearcanebrony.yatopiaupdater.JenkinsResponse;
import net.thearcanebrony.yatopiaupdater.LocalState;

import static net.thearcanebrony.yatopiaupdater.DownloadManager.DownloadFileNew;
import static net.thearcanebrony.yatopiaupdater.DownloadManager.GetUrlResponse;

public class JenkinsSource extends Source {

    public JenkinsSource(String name, String url) {
        super(name, url);
    }

    @Override
    public void DownloadLatest(String outfile) {
        Console.WriteLine(String.format("Fetching build info for %s...", Name));
        JenkinsResponse jenkinsResponse = JenkinsResponse.Get(GetUrlResponse(Url + "/api/json"));
        if(Config.LocalState == null) Config.LocalState = LocalState.Get();
        if(!Config.LocalState.builds.containsKey(Name)) Config.LocalState.builds.put(Name, new LocalState.Build());
        LocalState.Build b = Config.LocalState.builds.get(Name);
        Console.logDebug(String.format("%s -> %s", b.Number, jenkinsResponse.number));
        if(Config.LocalState.builds.get(Name).Number >= jenkinsResponse.number && Config.LocalState.LatestPull.equals(Name) && !Config.ForceDownload) {
            Console.WriteLine(Name + " is already up to date, not downloading!");
            return;
        }
        JenkinsResponse.Artifact artifact = jenkinsResponse.artifacts.get(0);
        String artifactUrl = Url + "/artifact/" + artifact.relativePath;
        Console.logDebug("Artifact URL: " + artifactUrl);
        Console.WriteLine(String.format("Got build info!\nDownloading latest %s jar...", Name));
        Long size = DownloadFileNew(artifactUrl, outfile);
        Console.LogIO("New download read: " + size);
        b.Number = jenkinsResponse.number;
        b.OutFile = outfile;
        if(!Name.equals("YatopiaUpdater")) Config.LocalState.LatestPull = Name;
    }
}
