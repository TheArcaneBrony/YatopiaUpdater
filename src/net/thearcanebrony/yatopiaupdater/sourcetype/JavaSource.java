package net.thearcanebrony.yatopiaupdater.sourcetype;

import net.thearcanebrony.yatopiaupdater.Config;
import net.thearcanebrony.yatopiaupdater.Console;
import net.thearcanebrony.yatopiaupdater.LocalState;
import net.thearcanebrony.yatopiaupdater.ZipManager;
import net.thearcanebrony.yatopiaupdater.ostype.OS;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static net.thearcanebrony.yatopiaupdater.DownloadManager.DownloadFileNew;

public class JavaSource extends Source {

    public JavaSource(String name, String url) {
        super(name, url);
    }

    @Override
    public void DownloadLatest(String outfile) {
        if(!Config.DownloadJava) return;
        Path javafolder = Paths.get("java");
        try {
            if(javafolder.toFile().exists() && Files.list(javafolder).findFirst().isPresent() && !Config.ForceDownload) {
                Console.WriteLine("Java is already downloaded, skipping!");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int version = 16;
        if(Config.LocalState == null) Config.LocalState = LocalState.Get();
        if(!Config.LocalState.builds.containsKey(Name)) Config.LocalState.builds.put(Name, new LocalState.Build());
        LocalState.Build b = Config.LocalState.builds.get(Name);
        Console.logDebug(String.format("%s -> %s", b.Number, version));
        if(Config.LocalState.builds.get(Name).Number >= version && Config.LocalState.LatestPull.equals(Name) && !Config.ForceDownload) {
            Console.WriteLine(Name + " is already up to date, not downloading!");
            return;
        }
        String platform = OS.getOS().OsType;
        Console.WriteLine(String.format("Downloading AdoptOpenJDK 16 for %s...", platform));
        DownloadFileNew(String.format("https://api.adoptopenjdk.net/v3/binary/latest/%s/ga/%s/x64/jre/hotspot/normal/adoptopenjdk?project=jdk",version,platform),outfile);
        ZipManager.Unzip("java.zip", "java");
        return;
//        Console.WriteLine(String.format("Fetching build info for %s...", Name));
//        JenkinsResponse jenkinsResponse = JenkinsResponse.Get(GetUrlResponse(Url + "/api/json"));
//        JenkinsResponse.Artifact artifact = jenkinsResponse.artifacts.get(0);
//        String artifactUrl = Url + "/artifact/" + artifact.relativePath;
//        Console.logDebug("Artifact URL: " + artifactUrl);
//        Console.WriteLine(String.format("Got build info!\nDownloading latest %s jar...", Name));
//        Long size = DownloadFileNew(artifactUrl, outfile);
//        Console.LogIO("New download read: " + size);
//        b.Number = jenkinsResponse.number;
//        b.OutFile = outfile;
//        if(!Name.equals("YatopiaUpdater")) Config.LocalState.LatestPull = Name;
    }
}
