package net.thearcanebrony.yatopiaupdater.sourcetype;

import net.thearcanebrony.yatopiaupdater.Console;

public class Source {
    public Source(String name, String url) {
        Name = name;
        Url = url;
    }

    public String Name = "";
    public String Url = "";
    public void DownloadLatest(String file){
        Console.WriteLine(this.getClass().getCanonicalName() + ": Download not implemented!");
    }
}
