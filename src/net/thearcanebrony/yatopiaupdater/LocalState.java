package net.thearcanebrony.yatopiaupdater;

import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class LocalState {
    private static Gson g = new Gson();
    public String LatestPull;

    public static LocalState Get(){
        if(!Files.exists(Paths.get("YatopiaUpdater.json"))) return new LocalState();
        else return g.fromJson(FileU.ReadAllText("YatopiaUpdater.json"), LocalState.class);
    }
    public void Save(){
        try {
            FileWriter myWriter = new FileWriter("YatopiaUpdater.json");
            myWriter.write(g.toJson(this));
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Map<String, Build> builds = new HashMap<String, Build>();
    public static class Build{
        public String Name = "";
        public String OutFile = "";
        public int Number = 0;
    }
}
