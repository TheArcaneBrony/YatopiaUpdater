package net.thearcanebrony.yatopiaupdater;

import net.thearcanebrony.yatopiaupdater.sourcetype.Source;

public class Config {
    public static boolean Verbose = false;
    public static boolean LogIO = false;
    public static String OutputFile = "server.jar";
    public static Source ServerSource = Sources.YATOPIA;
    public static LocalState LocalState = net.thearcanebrony.yatopiaupdater.LocalState.Get();
    public static boolean ForceDownload = false;
    public static boolean DownloadJava = true;
    public static boolean SystemJava = false;
    public static String LaunchMem = "1G";
    public static String ServerArgs = "";
    public static String JvmArgs = "";
    public static boolean AikarFlags = true;
}
