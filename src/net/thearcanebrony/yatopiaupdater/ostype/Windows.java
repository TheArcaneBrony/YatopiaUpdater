package net.thearcanebrony.yatopiaupdater.ostype;

public class Windows extends OS {
    {
        Name = "Windows";
        OsType = "windows";
        ShellCmd = new String[]{"cmd", "/c"};
    }
}
