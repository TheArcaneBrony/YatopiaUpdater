package net.thearcanebrony.yatopiaupdater.ostype;

import net.thearcanebrony.yatopiaupdater.Console;
import sun.awt.OSInfo;

public class OS {
    public static OS getOS(){
        switch(OSInfo.getOSType()){
            case LINUX:
                return new Linux();
            case WINDOWS:
                return new Windows();
            case MACOSX:
                return new MacOSX();
            case UNKNOWN:
            case SOLARIS:
            default:
                Console.WriteLine("Could not detect OS! Please report this to The Arcane Brony!");
                return new OS();
        }
    }
    public String Name = "Unknown OS, assuming Unix-compatible";
    public String OsType = "unknown";
    public String[] ShellCmd = {"bash", "-c"};
}
