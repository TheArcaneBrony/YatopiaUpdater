package net.thearcanebrony.yatopiaupdater;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ArgManager {
    private static int arg = 0;
    public static void init(ArrayList<String> args){
        if (args.contains("--verbose")) {
            Config.Verbose = true;
            Console.logDebug("Option: Verbose");
        }
        if (args.contains("--iolog")) {
            Config.Verbose = true;
            Config.LogIO = true;
            Console.logDebug("Option: IOLog");
        }
        if(args.contains("--lt")){
            String command = "uname -a";
            Console.WriteLine("Command being ran: " + command);
            Util.StartProcess(command);
            System.exit(0);
        }
        if (args.contains("--frd")) {
            Config.ForceDownload = true;
            Console.logDebug("Option: Force Re-Download");
        }
        if (args.contains("--help")) {
            Console.WriteLine("YatopiaUpdater help\n" +
                    "Supported arguments:\n" +
                    " --help        Shows this message\n" +
                    " --unstable    Use Hyalus instead of Yatopia\n" +
                    " --outfile     Change output filename\n" +
                    " --updateSelf  Update YatopiaUpdater\n" +
                    " --launch      Launches the server with java 16\n" +
                    " --skipjava    Skips java download (useful if you already have it downloaded here)\n" +
                    " --sysjava     Use current java runtime to launch server (implies --skipjava)\n" +
                    " --mem         Specify memory to use for server and launch (eg. 2G)\n" +
                    " --jvmargs     Specify JVM arguments for server (must be wrapped in \"\")\n" +
                    " --serverargs  Specify server arguments for server (must be wrapped in \"\")\n" +
                    "\n" +
                    "Debug arguments:\n" +
                    " --verbose     Increase verbosity\n" +
                    " --iolog       Enable I/O logging (implies --verbose)"
            );
            Console.logDebug("Help called, exiting!");
            System.exit(0);
        }
    }
    public static void predl(ArrayList<String> args){
        if(args.contains("--updateSelf")){
            Console.logDebug("Option: Update Self");
            Console.WriteLine("You are auto-updating YatopiaUpdater, bugs may arise!");
            Sources.YATOPIAUPDATER.DownloadLatest("YatopiaUpdater.jar");
            Config.LocalState.Save();
            args.remove("--updateSelf");
            Util.StartProcess(String.format("java -jar YatopiaUpdater.jar %s", String.join(" ", args)));
            System.exit(0);
        }
        if (args.contains("--unstable")) {
            Console.logDebug("Option: Unstable");
            Console.WriteLine("You are downloading Hyalus, keep in mind this is the beta version of Yatopia!");
            Config.ServerSource = Sources.HYALUS;
        }
        if (args.contains("--sysjava")) {
            Console.logDebug("Option: Use system java, implies --skipjava");
            Console.WriteLine("You are using the system java installation, issues may arise launching the server!");
            Config.SystemJava = true;
            Config.DownloadJava = false;
        }
        if (args.contains("--skipjava")) {
            Console.logDebug("Option: Don't redownload java");
            Config.DownloadJava = false;
        }
        if((arg = args.indexOf("--outfile")) >= 0){
            String newpath = args.get(arg+1);
            Console.logDebug("Option: Change output file: "+ newpath);
            Config.OutputFile = newpath;
        }
        if((arg = args.indexOf("--mem")) >= 0){
            String mem = args.get(arg+1);
            Console.logDebug("Option: Memory: "+ mem);
            Config.LaunchMem = mem;
        }
        if((arg = args.indexOf("--jvmargs")) >= 0){
            String str = args.get(arg+1);
            Console.logDebug("Option: JVM Arguments: "+ str);
            Config.JvmArgs = str;
        }
        if((arg = args.indexOf("--serverargs")) >= 0){
            String str = args.get(arg+1);
            Console.logDebug("Option: Server Arguments: "+ str);
            Config.ServerArgs = str;
        }
        if (args.contains("--no-aikar-flags")) {
            Console.logDebug("Option: Don't use Aikar's flags");
            Console.WriteLine("You are not using Aikar's flags - https://aikar.co/2018/07/02/tuning-the-jvm-g1gc-garbage-collector-flags-for-minecraft/\n" +
                    "Please note that these flags are recommended and help with the performance and ram usage\n" +
                    "If you want the best performance with the lowest RAM usage, please consider re-enabling these flags.");
            Config.AikarFlags = false;
        }
    }
    public static void postdl(ArrayList<String> args){
        if ((arg = args.indexOf("--launch")) >= 0 || args.contains("--mem")) {
            //download java 16
            Sources.ADOPTOPENJDK.DownloadLatest("java.zip");
            String javahome = System.getProperty("java.home");
            if(!Config.SystemJava) {
                try {
                    javahome = "./" + Files.list(Paths.get("java")).findFirst().get().toString();
                } catch (IOException e) {
                    //
                }
            }
            String m = Config.LaunchMem;
            String ja = String.format("%s%s",Config.AikarFlags?Static.AikarFlags:"",Config.JvmArgs);
            Util.StartProcess(String.format("\"%s/bin/java\" -Xms%s -Xmx%s %s -jar %s %s",javahome,m,m,ja,Config.OutputFile,Config.ServerArgs));
            //run server
        }
    }
}
