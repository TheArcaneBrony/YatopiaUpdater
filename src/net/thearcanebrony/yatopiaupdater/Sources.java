package net.thearcanebrony.yatopiaupdater;

import net.thearcanebrony.yatopiaupdater.sourcetype.JavaSource;
import net.thearcanebrony.yatopiaupdater.sourcetype.JenkinsSource;

public class Sources {
    public static JenkinsSource YATOPIAUPDATER = new JenkinsSource("YatopiaUpdater", "http://thearcanebrony.net:8080/job/YatopiaUpdater/lastStableBuild");
    public static JenkinsSource YATOPIA = new JenkinsSource("Yatopia", "https://ci.codemc.io/job/YatopiaMC/job/Yatopia/job/ver%252F1.16.5/lastStableBuild");
    public static JenkinsSource HYALUS = new JenkinsSource("Hyalus", "https://www.gardling.com/jenkins/job/Hyalus/lastStableBuild/");
    public static JavaSource ADOPTOPENJDK = new JavaSource("AdoptOpenJDK", "");
}
