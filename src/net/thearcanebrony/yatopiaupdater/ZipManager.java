package net.thearcanebrony.yatopiaupdater;

import java.io.*;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class ZipManager {
    private static ZipEntry zipEntry;
    private static long extractedBytes = 0, fileCount = 0, totalFileCount = 0;
    public static void Unzip(String filename, String outDir){
        try {
            final String fileZip = filename;
            final File destDir = new File(outDir);
            final byte[] buffer = new byte[1024];
            ZipFile zf = new ZipFile(filename);
            totalFileCount = zf.size();
            zf.close();
            final ZipInputStream zis = new ZipInputStream(new FileInputStream(filename));

            zipEntry = zis.getNextEntry();
            new Thread(){
                @Override
                public void run() {
                    String[] Spinner = "|/-\\".split("");
                    String e = "              ";
                    int cooldown = 50;
                    double ups = 1000d/cooldown;
                    double[] _dls = new double[(int) Math.ceil(ups)];
                    int i = 0;
                    int SP = 0;
                    long lastUpdate = 0;
                    while(zipEntry != null){
                        String sp = Spinner[SP++%Spinner.length];
                        _dls[(int) (i++%ups)] = extractedBytes - lastUpdate;
                        double dls = Arrays.stream(_dls).sum();
                        double pct = fileCount/(double)totalFileCount;
                        if(Config.LogIO){
                            Console.Write(String.format("%s Read %s/%s files containing %d bytes (%.2f%%) at %.2f MB/s%s\r", sp, fileCount, totalFileCount, extractedBytes, pct, dls, e));
                        }
                        else {
                            Console.Write(String.format("%s Read %s/%s files containing %.2f MB (%.2f%%) at %.2f MB/s%s\r", sp, fileCount, totalFileCount, extractedBytes/1024d/1024d, pct, dls/1024d/1024d, e));
                        }
                        lastUpdate = extractedBytes;
                        try {
                            Thread.sleep(cooldown);
                        } catch (InterruptedException ie) {
                            Thread.currentThread().interrupt();
                        }
                    }
                }
            }.start();
            while (zipEntry != null) {
                final File newFile = newFile(destDir, zipEntry);
                if (zipEntry.isDirectory()) {
                    if (!newFile.isDirectory() && !newFile.mkdirs()) {
                        throw new IOException("Failed to create directory " + newFile);
                    }
                } else {
                    File parent = newFile.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("Failed to create directory " + parent);
                    }

                    final FileOutputStream fos = new FileOutputStream(newFile);
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                        extractedBytes += len;
                    }
                    fos.close();
                }
                zipEntry = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
            Console.WriteLine(String.format("Extraction complete! Extracted %s into %s!            ",Config.LogIO?(extractedBytes + " bytes"):(String.format("%.2f", extractedBytes/1024d/1024d) + " MB"),outDir));
        }
        catch(IOException e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Console.WriteLine("Something went wrong!\n" + sw);
        }
    }

    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        fileCount++;
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }
}
