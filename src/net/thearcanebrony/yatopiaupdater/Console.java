package net.thearcanebrony.yatopiaupdater;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Console {
    private static int maxSrcLength = 0;
    private static SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
    public static void Write(String txt){
        String[] lines = txt.split("\n");
        txt = "";
        for (String line : lines) {
            txt += String.format("[%s]: %s\n", df.format(new Date()), line);
        }
        System.out.print(txt.substring(0,txt.length()-1));
    }
    public static void WriteLine(String txt) {
        String[] lines = txt.split("\n");
        txt = "";
        for (String line : lines) {
            txt += String.format("[%s]: %s\n", df.format(new Date()), line);
        }
        System.out.print(txt);
    }
    public static void logDebug(String text){
        if(Config.Verbose) {
            StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
            String[] lines = text.split("\n");
            StringBuilder source = new StringBuilder(ste.getFileName() + ":" + ste.getLineNumber());
            if(source.length() >= maxSrcLength)
                maxSrcLength = source.length();
            else while (source.length() < maxSrcLength)
                source.append(" ");
            text = "";
            for (String line : lines) {
                text += String.format("%s -> %s\n", source, line);
            }
            WriteLine(text);
        }
    }
    public static void LogIO(String text){
        if(Config.LogIO) {
            StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
            String[] lines = text.split("\n");
            StringBuilder source = new StringBuilder(ste.getFileName() + ":" + ste.getLineNumber());
            if(source.length() >= maxSrcLength)
                maxSrcLength = source.length();
            else while (source.length() < maxSrcLength)
                source.append(" ");
            text = "";
            for (String line : lines) {
                text += String.format("%s -> %s\n", source, line);
            }
            WriteLine(text);
        }
    }
}
